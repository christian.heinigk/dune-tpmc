install(FILES AddTpmcFlags.cmake
              DuneTpmcMacros.cmake
              FindSFML.cmake
              FindTpmc.cmake
              SFMLLicense.txt
        DESTINATION ${DUNE_INSTALL_MODULEDIR})
