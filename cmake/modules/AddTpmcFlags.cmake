
#
# Module providing convenience methods for compile binaries with Tpmc support.
#
# Provides the following functions:
#
# add_dune_tpmc_flags(target1 target2 ...)
#
# adds Tpmc flags to the targets for compilation and linking
#
function(add_dune_tpmc_flags)
  if(TPMC_FOUND)
    include(CMakeParseArguments)
    cmake_parse_arguments(ADD_TPMC "OBJECT;SOURCE_ONLY;PRIVATE;INTERFACE;PUBLIC" "" "" ${ARGN})
    if(ADD_TPMC_PRIVATE)
      set(_visibility PRIVATE)
    elseif(ADD_TPMC_INTERFACE)
      set(_visibility INTERFACE)
    elseif(ADD_TPMC_PUBLIC)
      set(_visibility PUBLIC)
    endif()
    if(ADD_TPMC_SOURCE_ONLY)
      set(_prefix SOURCE)
      set(_source_only SOURCE_ONLY)
      foreach(_target ${ADD_TPMC_UNPARSED_ARGUMENTS})
        target_include_directories(${_target} ${_visibility} ${TPMC_INCLUDE_DIRS})
      endforeach(_target ${ADD_TPMC_UNPARSED_ARGUMENTS})
    else(ADD_TPMC_SOURCE_ONLY)
      if(NOT ADD_TPMC_OBJECT)
        foreach(_target ${ADD_TPMC_UNPARSED_ARGUMENTS})
          target_link_libraries(${_target} ${_visibility} ${TPMC_LIBRARIES})
          target_include_directories(${_target} ${_visibility} ${TPMC_INCLUDE_DIRS})
        endforeach(_target ${ADD_TPMC_UNPARSED_ARGUMENTS})
      endif(NOT ADD_TPMC_OBJECT)
      set(_prefix TARGET)
      set_property(${_prefix}  ${ADD_TPMC_UNPARSED_ARGUMENTS} APPEND
        PROPERTY
        COMPILE_DEFINITIONS ENABLE_TPMC)
    endif(ADD_TPMC_SOURCE_ONLY)
  endif(TPMC_FOUND)
endfunction(add_dune_tpmc_flags)
