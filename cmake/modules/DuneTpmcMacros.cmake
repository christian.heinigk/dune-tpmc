# File for module specific CMake tests.
include(FindTpmc)
find_package(Tpmc REQUIRED)
include(AddTpmcFlags)
