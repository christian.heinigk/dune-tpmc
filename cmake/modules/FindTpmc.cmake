# Module that checks whether tpmc is available
#
# Accepts the following input variable
# TPMC_PREFIX: Prefix under which tpmc is installed
#
# The following variable will be set:
# TPMC_FOUND: whether tpmc is available
# TPMC_INCLUDE_DIRS: Include directories for tpmc
# TPMC_LIBRARIES: Full path to libraries needed to link
#   to tpmc
#
set(TPMC_FOUND TPMC_FOUND-NOTFOUND)

# find header in user supplied directory
find_path(TPMC_INCLUDE_DIR tpmc/marchinglut.hh
  PATHS ${TPMC_PREFIX}
  PATH_SUFFIXES include include/python tpmc/include
  NO_DEFAULT_PATH
  DOC "Include directory with tpmc header files")
find_path(TPMC_INCLUDE_DIR tpmc/marchinglut.hh
  PATH_SUFFIXES include include/python tpmc/include
  DOC "Include directory with tpmc header files")

# find library in custom directory
find_library(TPMC_LIBRARY
  NAMES tpmc_tables
  PATHS ${TPMC_PREFIX}
  PATH_SUFFIXES lib lib/python/tpmc/lib
  NO_DEFAULT_PATH
  DOC "Full path to tpmc library.")

# find lib in default directory
find_library(TPMC_LIBRARY
  NAMES tpmc_tables
  PATH_SUFFIXES lib lib/python/tpmc/lib)

set(TPMC_INCLUDE_DIRS ${TPMC_INCLUDE_DIR})
set(TPMC_LIBRARIES ${TPMC_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  "tpmc"
  DEFAULT_MSG
  TPMC_INCLUDE_DIR
  TPMC_LIBRARY
)
mark_as_advanced(TPMC_INCLUDE_DIR TPMC_LIBRARY)
