// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TPMC_HH
#define DUNE_TPMC_HH

#include <dune/tpmc/tpmcrefinement.hh>
#include <dune/tpmc/nondegeneratingthresholdfunctor.hh>

#endif  // DUNE_TPMC_HH
