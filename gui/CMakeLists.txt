find_package(wxWidgets)
find_package(SFML COMPONENTS system window graphics)
find_package(PkgConfig)

if (PKGCONFIG_FOUND)
  pkg_check_modules(GTK2 gtk+-2.0)
endif()

if (SFML_FOUND AND wxWidgets_FOUND AND GTK2_FOUND)
  include(${wxWidgets_USE_FILE})
  include_directories(${GTK2_INCLUDE_DIRS})
  add_executable(dune_mc_gui dune_mc_gui.cc wxsfmlcanvas.cpp)
  target_link_libraries(dune_mc_gui ${wxWidgets_LIBRARIES})
  target_link_libraries(dune_mc_gui ${SFML_LIBRARIES})
  target_link_libraries(dune_mc_gui ${OPENGL_LIBRARIES})
  target_link_libraries(dune_mc_gui ${GTK2_LIBRARIES})
  target_link_dune_default_libraries(dune_mc_gui)
  add_dune_alugrid_flags(dune_mc_gui)
  add_dune_ug_flags(dune_mc_gui)
  add_dune_tpmc_flags(dune_mc_gui)
  if(SUPERLU_FOUND)
    add_dune_superlu_flags(dune_mc_gui)
  endif(SUPERLU_FOUND)
endif()
